package com.rediff.RediffTesting;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.utils.Common;
import com.utils.ReadExcel;

public class RediffLoginTest extends Common {
//Comment in LoginTest
	@BeforeSuite
	public void initializeFiles() throws IOException {
		loadPropFiles();
	}

	@BeforeMethod
	public void openBrowser() {
		openBrowsers();
	}

	@Test(priority = 1)
	public void verifyPageTitle() throws IOException {
		try {
			String actualPageTitle = driver.getTitle();
			String expectedPageTitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
			Assert.assertEquals(actualPageTitle, expectedPageTitle);
			log.debug("Verified the page title");
			int titleCount = actualPageTitle.length();
			log.debug("titleCount: " + titleCount);
			// Assert.assertTrue(titleCount >= 100, "Title char count is not >= 100");

			// Assert.assertFalse(titleCount <= 100,"Title char count is not >= 100");
		} catch (Exception e) {
			captureScreenshot();
			e.printStackTrace();
			log.debug(e.getMessage());
		}
	}

	@Test(priority = 2, dependsOnMethods = "verifyPageTitle", enabled = true, dataProvider = "data")
	public void loginWithInvalidCredentials(String username, String password) throws InterruptedException, IOException {
		try {
			driver.findElement(By.linkText("Sign in")).click();
			Thread.sleep(1500);
			// 3. enter email as 'sdfds@sdfds.com'
			// driver.findElement(By.id("txtlogin")).sendKeys("sdfds@sdfds.com");
			driver.findElement(By.xpath(obj.getProperty("loginInputField"))).sendKeys(username);

			// 4. enter invalid pass as 'sdfdsfdsf'
			driver.findElement(By.id(obj.getProperty("passwordInputField"))).sendKeys(password);
			// 5. Click on Login button
			driver.findElement(By.name(obj.getProperty("signInButton"))).click();

			// 6. extract the error message
			String actualErrorMessage = driver.findElement(By.xpath(obj.getProperty("errorMessage"))).getText();
			String expectedErrorMessage = "Wrong username and password combination.";

			System.out.println(actualErrorMessage);
			System.out.println(expectedErrorMessage);

			// 7. verify the error message
			if (actualErrorMessage.equals(expectedErrorMessage)) {
				System.out.println("Messgaes are matching");
			} else {
				System.out.println("Messgaes are not matching");
			}
			log.debug("Verified Login with invalid credentials: " + username + "-- " + password);
		} catch (Exception e) {
			captureScreenshot();
			e.printStackTrace();
			log.debug(e.getMessage());
		}
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException {

		ReadExcel excel = new ReadExcel();
		Object o[][] = excel.readExcelData();
		log.debug("Take the test data form ReadExcel method and passing it to test method");
		return o;
	}

}
