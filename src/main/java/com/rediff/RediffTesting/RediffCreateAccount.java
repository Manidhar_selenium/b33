package com.rediff.RediffTesting;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.utils.Common;

public class RediffCreateAccount extends Common {
	@BeforeSuite
	public void initializeFiles() throws IOException {
		loadPropFiles();
	}

	@BeforeMethod
	public void openBrowser() {
		openBrowsers();
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

	@Test
	public void verifyCreateAccount() throws InterruptedException, IOException {
		try {
		driver.findElement(By.linkText(obj.getProperty("createAccountLink"))).click();

		driver.findElement(By.xpath(obj.getProperty("fullNameInputField")))
				.sendKeys("Manidhar Kumar");
		driver.findElement(By.xpath(obj.getProperty("rediffMailIdInputField")))
				.sendKeys("ashok");
		driver.findElement(By.xpath(obj.getProperty("checkAvailabilityButton")))
				.click();

		Thread.sleep(1500);

		String actualErrorMessage = driver.findElement(By.xpath(obj.getProperty("CheckAvailabilityErrorMessage"))).getText();
		String expectedErrorMessage = "Sorry, the ID that you are looking for is taken.";

		Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
		log.debug("Verified that user email id is available");
		}catch(Exception e) {
			captureScreenshot();
			e.printStackTrace();
		}
	}

}
