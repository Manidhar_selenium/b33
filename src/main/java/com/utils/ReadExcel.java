package com.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel extends Common{

	public Object[][] readExcelData() throws EncryptedDocumentException, IOException {
		InputStream fis = new FileInputStream(
				path+"/src/main/resources/LoginTestData.xlsx");
		Workbook workbook = WorkbookFactory.create(fis);
		Sheet sheet = workbook.getSheetAt(0);

		int totalRows = sheet.getLastRowNum() + 1;

		System.out.println("Total rows in the sheet are: " + totalRows);

		Row row = sheet.getRow(0);
		int totalColumns = row.getLastCellNum();

		Object o[][] = new Object[totalRows][totalColumns];

		
		//row
		for (int i = 0; i < totalRows; i++) {
			row = sheet.getRow(i);
			totalColumns = row.getLastCellNum();
			
			//cells in selected row
			for (int j = 0; j < totalColumns; j++) {
				Cell cell=row.getCell(j);
				o[i][j]=cell.getStringCellValue();
			}
		}
		//log.debug("Reading the data from Excel and pass to caller of the method");
		return o;
	}

}
