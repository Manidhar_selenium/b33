package com.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Common {
	public WebDriver driver = null;
	public Properties env = null;
	public Properties obj = null;
	public Logger log = null;
	public String path=System.getProperty("user.dir");

	public void loadPropFiles() throws IOException {
		System.out.println(path);
		log = Logger.getLogger("rootLogger");
		InputStream fis = new FileInputStream(
				path+"/src/main/resources/Environment.properties");
		env = new Properties();
		env.load(fis);
		log.debug("Environment.properties file is loaded");

		InputStream objsFile = new FileInputStream(
				path+"/src/main/resources/Objects.properties");
		obj = new Properties();
		obj.load(objsFile);
		log.debug("Objects.properties file is loaded");
	}

	public void openBrowsers() {
		// 1. open the browser
		System.setProperty("webdriver.chrome.driver",
				path+"/src/main/resources/drivers/mac/chromedriver");
		driver = new ChromeDriver();
		driver.get(env.getProperty("url"));
		log.debug("Opening Chrome Browser");
	}

	public void closeBrowsers() {
		// 8. close the browser
		driver.close();
		log.debug("Closing the Browser");
	}

	public void captureScreenshot() throws IOException {
		TakesScreenshot s=((TakesScreenshot)driver);
		File f=s.getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(f, new File(path+"/src/main/resources/screenshots/Screenshot.png"));
	}
}
